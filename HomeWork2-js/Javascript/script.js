"use strict";

let a=parseFloat(prompt("Please enter number1:"));
let b=parseFloat(prompt("Please enter number2: "));
let char=prompt("Please enter operator:");

function isValidNumber(x) {
    if(!isNaN(x) && x!="" && x!=null)
        return true;
    return false;
}
function isValidChar(x) {
    if(x==="*"||x==="/"||x==="+"||x==="-")
        return true;
    return false;
}
while (!(isValidNumber(a)&&isValidNumber(b)))
{
    a=parseFloat(prompt("Please enter valid number1:",a));
    b=parseFloat(prompt("Please enter valid number2:",b));
}
while (!isValidChar(char))
{
    char=prompt("Please enter valid character");
}
function calculate(a,b,ch) {
    let res;
    switch (ch) {
        case "+":
            res=a+b;
            break;
        case "-":
            res=a-b;
            break;
        case "*":
            res=a*b;
            break;
        case "/":
            res=a/b;
            break;
    }
    return res;
}

console.log(calculate(a, b, char));